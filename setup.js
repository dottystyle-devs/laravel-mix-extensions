"use strict";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var mix = require('laravel-mix');

var argv = require('yargs').argv;

var CleanWebpackPlugin = require('clean-webpack-plugin');

var shouldAddVersion = function shouldAddVersion(options) {
  return mix.inProduction() && (options.versioned || _typeof(argv.env) === 'object' && argv.env.versioned);
};

var SetupExtension =
/*#__PURE__*/
function () {
  function SetupExtension() {
    _classCallCheck(this, SetupExtension);
  }

  _createClass(SetupExtension, [{
    key: "name",
    value: function name() {
      return 'setup';
    }
  }, {
    key: "register",
    value: function register() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (_typeof(options.cleanOnProd) === 'object') {
        this.cleanOnProd = _objectSpread({
          paths: [],
          options: {}
        }, options.cleanOnProd);
      }

      if (shouldAddVersion(options)) {
        mix.version();
      }
    }
  }, {
    key: "webpackPlugins",
    value: function webpackPlugins() {
      var plugins = []; // Clean files when building for production

      if (this.cleanOnProd) {
        var _this$cleanOnProd = this.cleanOnProd,
            paths = _this$cleanOnProd.paths,
            cleanOptions = _objectWithoutProperties(_this$cleanOnProd, ["paths"]);

        plugins.push(new CleanWebpackPlugin(paths, cleanOptions));
      }

      return plugins;
    }
  }]);

  return SetupExtension;
}();

mix.extend('setup', new SetupExtension());